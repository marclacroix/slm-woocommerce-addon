<?php
add_filter('slm_add_edit_interface_above_submit', 'slm_wooaddon_custom_output', 10, 2);
function slm_wooaddon_custom_output($output, $data){
    //$row_id = $data['row_id'];
    //$key = $data['key'];
    //TODO - query to retrieve your extra data so you can create the HTML output here
	 //If product is being edited, grab current product info
	global $wpdb;
	
	//Edit Record
    if (isset($_GET['edit_record'])) {

        $errors = '';

        $id = $_GET['edit_record'];

        $lk_table = SLM_TBL_LICENSE_KEYS;

        $sql_prep = $wpdb->prepare("SELECT * FROM $lk_table WHERE id = %s", $id);

        $record = $wpdb->get_row($sql_prep, OBJECT);

        $product_name = $record->product_name;
		
		$product_id = $record->product_id;
		
		$variation_id = $record->variation_id;
		
		$variation_attribute = $record->variation_attribute;

    }
	
	//Save Record
	if (isset($_POST['save_record'])) {

        //TODO - do some validation

        $product_name = $_POST['product_name'];
		
		$product_id = $_POST['product_id'];
		
		$variation_id = $_POST['variation_id'];
		
		$variation_attribute = $_POST['variation_attribute'];

        //Save the entry to the database

        $fields = array();

        $fields['product_id'] = $product_id;
		
		$fields['variation_id'] = $variation_id;
		
		$fields['variation_attribute'] = $variation_attribute;
		
		$fields['product_name'] = $product_name;

        $id = isset($_POST['edit_record'])?$_POST['edit_record']:'';

        $lk_table = SLM_TBL_LICENSE_KEYS;

        if (empty($id)) {//Insert into database

            $result = $wpdb->insert( $lk_table, $fields);

            $id = $wpdb->insert_id;

        } else { //Update record

            $where = array('id'=>$id);

            $updated = $wpdb->update($lk_table, $fields, $where);

        }       

    }
	
	$output .= '<table class="form-table">';
	$output .= '<tbody>';
	$output .= '<tr valign="top">';

    $output .= '<th scope="row">Product Name</th>';

    $output .= '<td><input name="product_name" type="text" id="product_name" value="' . $product_name . '" size="30" />';

    $output .= '<br/>Name of the product licensed</td>';

    $output .= '</tr>';
	$output .= '<tr valign="top">';

    $output .= '<th scope="row">Product ID</th>';

    $output .= '<td><input name="product_id" type="text" id="product_id" value="' . $product_id . '" size="6" />';

    $output .= '<br/>ID of the product licensed</td>';

    $output .= '</tr>';
	$output .= '<tr valign="top">';

    $output .= '<th scope="row">Product Variation ID</th>';

    $output .= '<td><input name="variation_id" type="text" id="variation_id" value="' . $variation_id . '" size="6" />';

    $output .= '<br/>Variation ID of the product licensed. <i style="font-size:11px;">This is for a variable product</i></td>';

    $output .= '</tr>';
	$output .= '<tr valign="top">';

    $output .= '<th scope="row">Product Variation Attribute</th>';

    $output .= '<td><input name="variation_attribute" type="text" id="variation_attribute" value="' . $variation_attribute . '" size="30" />';

    $output .= '<br/>Type of subscription for the product. <i style="font-size:11px;">This is for a variable product</i></td>';

    $output .= '</tr>';
	$output .= '</tbody>';
	$output .= '</table>';

    return $output;
}
?>