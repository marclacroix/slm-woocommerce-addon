<?php
global $wpdb;

//Add to field to license key table if doesn't exist.
$lic_key_table = SLM_TBL_LICENSE_KEYS;

// Changed the query to find out if the column exists. Returns TRUE or FALSE, not an array anymore.
$row = $wpdb->get_row("SHOW COLUMNS FROM {$lic_key_table} LIKE 'product_id'" );
if( NULL === $row ){
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN product_id varchar(64) NOT NULL after txn_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN variation_id varchar(64) NOT NULL after product_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN product_name varchar(64) NOT NULL after variation_id");
	$wpdb->query("ALTER TABLE {$lic_key_table} ADD COLUMN variation_attribute varchar(64) NOT NULL after product_name");
}
?>