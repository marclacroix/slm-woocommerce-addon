<?php
//Add License Checkbox to Product License tab.
add_filter( 'woocommerce_product_data_tabs', 'slm_wooaddon_add_product_license_data_tab' , 99 , 1 );
function slm_wooaddon_add_product_license_data_tab( $product_data_tabs ) {
	$product_data_tabs['product-license-tab'] = array(
		'class' => array('show_if_simple', 'show_if_simple'),
		'label' => __( 'Product License', 'slm_wooaddon' ),
		'target' => 'product_license_data',
	);
	return $product_data_tabs;
}

add_action( 'woocommerce_product_data_panels', 'slm_wooaddon_add_product_license_data_fields' );
function slm_wooaddon_add_product_license_data_fields() {
	global $woocommerce, $post;
	?>
	<!-- id below must match target registered in above add_product_license_data_tab function -->
	<div id="product_license_data" class="panel woocommerce_options_panel">
	<p><?php 
		_e('If this downloadable product will have a license key, then check the following box.','slm_wooaddon'); 

		 woocommerce_wp_checkbox( array(
			'id' => '_product_license',
			'label' => __('Product License','slm_wooaddon'),
			'description' => __('Does this product include a license','slm_wooaddon'),
			'desc_tip' => 'true',
			'placeholder' => __('License','slm_wooaddon'),
			'default'       => 'no'
		) );
		?>
	</div>
	<?php
}


//Save License Checkbox
add_action( 'woocommerce_process_product_meta', 'slm_wooaddon_custom_save_custom_fields' );
function slm_wooaddon_custom_save_custom_fields( $post_id ) {
	update_post_meta( $post_id, '_product_license', esc_attr( $_POST['_product_license'] ) );
}

//Add License count input field to variable product.
add_action( 'woocommerce_product_after_variable_attributes', 'slm_wooaddon_add_to_variations_metabox', 10, 3 );

function slm_wooaddon_add_to_variations_metabox($loop, $variation_data, $variation){
	
	$product_license_count = get_post_meta( $variation->ID, '_variable_is_product_license_count', true );
	?>
	<p class="form-row form-row-first">
	<label><?php _e( 'Product License Count', 'slm_wooaddon' ); ?>
	<a class="tips" data-tip="<?php esc_attr_e( 'Enter the amount of licenses this item price will have', 'slm_wooaddon' ); ?>" href="#"><span class="woocommerce-help-tip"></span></a></label>
		<input type="number" class="short" name="variable_is_product_license_count[<?php echo $loop; ?>]" value="<?php echo $product_license_count; ?>" /> 
		</p>
	<?php
}


/*
* add options variation
*/
add_action( 'woocommerce_variation_options', 'slm_wooaddon_woocommerce_add_variation_options' , 10, 3 );
function slm_wooaddon_woocommerce_add_variation_options( $loop, $variation_data, $variation ) {
	
	$product_license = get_post_meta( $variation->ID, '_variable_is_product_license', true );
	?>
	<label>
		<input type="checkbox" class="checkbox" name="variable_is_product_license[<?php echo $loop; ?>]" <?php checked( $product_license, 'yes' ); ?> /> <?php _e( 'Product License', 'slm_wooaddon' ); ?>
	<a class="tips" data-tip="<?php esc_attr_e( 'Does this product include a license', 'slm_wooaddon' ); ?>" href="#"><span class="woocommerce-help-tip"></span></a>
	</label>
	<?php
}

/*
* save add ons of product variation
*/
add_action( 'woocommerce_save_product_variation', 'slm_wooaddon_woocommerce_save_added_product_variation', 10, 2 );
function slm_wooaddon_woocommerce_save_added_product_variation( $variation_id, $i ) {
	
	$variable_is_product_license = isset( $_POST['variable_is_product_license'] ) ? $_POST['variable_is_product_license'] : array();
	for ( $count = 0; $count <= $i; $count ++ ) {
		$product_license = isset( $variable_is_product_license[ $count ] ) ? 'yes' : 'no';
		update_post_meta( $variation_id, '_variable_is_product_license', wc_clean( $product_license ) );
	}
	
	$variable_is_product_license_count = $_POST['variable_is_product_license_count'];
	for ( $count = 0; $count <= $i; $count ++ ) {
		$product_license_count = $variable_is_product_license_count[ $count ];
		if(isset( $variable_is_product_license_count)){
			update_post_meta( $variation_id, '_variable_is_product_license_count', wc_clean( $product_license_count ) );
		}
	}
}

?>