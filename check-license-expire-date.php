<?php
// WP MAIL HTML FILTER
add_filter( 'wp_mail_content_type', 'set_content_type' );
function set_content_type( $content_type ) {
	return 'text/html';
}
//WP MAIL FROM EMAIL
add_filter( 'wp_mail_from', 'my_mail_from' );
function my_mail_from( $email )
{
	return get_bloginfo('admin_email');
}

//WP MAIL FROM NAME
add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
function my_mail_from_name( $name )
{
	return get_bloginfo('name');
}

function slm_wooaddon_check_license_expire_date () {
	global $wpdb;
	$license_table = SLM_TBL_LICENSE_KEYS;
	$myArray = $wpdb->get_results("SELECT * FROM " . $license_table);
	$newArray=array();					
	foreach($myArray as $val){
		$newKey=$val->txn_id;
    	$newArray[$newKey][]=$val;
	}
	
	foreach ($newArray as $key => $array){
		foreach($array as $values) {
			$licenseKey = $values->license_key;
			$product_id = $values->product_id;
			$product_name = $values->product_name;
			$FirstName = $values->first_name;
			$customerEmail = $values->email;
			
			$expireDate = $values->date_expiry;
							
			$currentDate = date('Y-m-d');
			$monthBefore = strtotime('-1 month', strtotime($expireDate));
			
			$monthBefore = date ('Y-m-d', $monthBefore);
			$twoWeeksBefore = strtotime('-2 weeks', strtotime($expireDate));
			$twoWeeksBefore = date ('Y-m-d', $twoWeeksBefore);
			$threeDaysBefore = strtotime('-3 days', strtotime($expireDate));
			$threeDaysBefore = date ('Y-m-d', $threeDaysBefore);
			
			$email_subject = __('Action Needed: subscription expires soon','slm_wooaddon');
			
			if($currentDate == $monthBefore || $currentDate == $twoWeeksBefore || $currentDate == $threeDaysBefore){
			ob_start();
			?>
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			 <head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title><?php bloginfo('name'); ?> Plugin Subscription Renewal</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
			  <style>
				table {
					border-collapse: collapse;
				}
				table tr.header td {
					padding: 10px 18px 8px 20px;
					text-align: left; 
					background-color: rgb(247, 247, 247);
					border: 1px solid #E6E6E6;
				}
				table tr.details td {
					padding: 10px 18px 8px 20px;
					text-align: left; 
					background-color: rgb(255, 255, 255);
					border: 1px solid #E6E6E6;
				}
				a.renew_button {
					background: #428bca none repeat scroll 0 0 !important;
					border-color: #357ebd !important;
					color: #fff !important;
					text-decoration:none !important;
					text-align:center;
					display: block !important;
					padding: 20px 0 !important;
					width: 100% !important;
					text-align:center;
					font-size:18px;
				}
			  </style>
			</head>
			<body style="margin: 0; padding: 0;">
			
				<?php
				// Get the template
				$template = 'renew-email-template.php';
			 
				// Check if a custom template exists in the theme folder, if not, load the plugin template file
				if ( $theme_file = locate_template( array( 'slm-addon/' . $template ) ) ) {
					include(get_template_directory().'/slm-addon/'.$template);
				}
				else {
					include(plugin_dir_path( __FILE__ ).'/templates/'.$template);
				}//end if local email template
				?>
			</body>
			</html>    
			<?php
			
			$message = ob_get_contents();
			ob_end_clean();
			//echo $message;
			wp_mail($customerEmail, $email_subject, $message);
		 	}//end if 1 month, 2 weeks or 3 days before expire date.
		}// end foreach array
	}//end foreach newArray
	
}// end function check_license_expire_date
add_action('check_license_expire_date', 'slm_wooaddon_check_license_expire_date');
?>