��          �   %   �      p     q  (   �     �     �  *   �  "   �  #     6   @  K   w     �     �     �  S   �     F     K     S     _     z     �     �     �     �     �     �     �  >   �  >   "  y  a     �  -   �  	   (     2  5   O  +   �     �  1   �  i   	     l	     �	     �	  ]   �	     �	     
     
     
  	   ;
  
   E
     P
     X
     h
     y
     �
     �
  E   �
  7   �
                                                                    	                                                   
    APPLY LICENSE RENEWAL Action Needed: subscription expires soon Cancel Cancel License Renewal Click %s to expand and see active domains. Click to renew an existing license Does this product include a license Enter the amount of licenses this item price will have Enter the license key you wish to renew. Leave blank to purchase a new one. Enter your license key Expire Date Hi there %s If this downloadable product will have a license key, then check the following box. Item License License Key License Keys being renewed Licenses Order Product Product License Product License Count Renew License Renewing a license key? Sites We noticed that one of your %s subscriptions will expire soon: You currently do not have any plugins purchased with licenses. Project-Id-Version: SLM WooCoomerce Addon
POT-Creation-Date: 2016-08-06 12:05+0100
PO-Revision-Date: 2016-08-06 12:06+0100
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.3
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 LICENTIE VERNIEUWING TOEPASSEN Actie vereist: abonnement verloopt binnenkort Annuleren Licentieverlenging annuleren Klik %s om uit te vouwen en actieve domeinen te zien. Klik om een bestaande licentie te verlengen Bevat dit product een licentie Geef het aantal licenties op dat deze prijs heeft Voer de licentiesleutel in die u wilt vernieuwen. Laat dit veld leeg om een ​​nieuwe aan te schaffen. Voer uw licentiesleutel in Verloopdatum Hallo %s Als dit downloadbare product een licentiesleutel krijgt, vink dan het volgende keuzeveld aan. Artikel Licentie Licentiesleutel Te vernieuwen licentiesleutels Licenties Bestelling Product Productlicentie Aantal licenties Licentie vernieuwen Licentiesleutel vernieuwen? Sites We hebben gemerkt dat een van uw% s abonnementen binnenkort verloopt: U heeft momenteel geen producten gekocht met licenties. 