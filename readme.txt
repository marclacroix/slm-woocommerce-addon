=== SLM WooCommerce Addon ===
Contributors: Tyler Robinson
Tags: license key, serial key, manager, license, serial, key, selling, sell, license activation, manage license, software license, software license manager, woocommerce
Requires at least: 3.0
Tested up to: 4.6.1
Stable tag: 2.0
License: GPLv2 or later

Create license keys for your software applications with Software License Manager when purchased through your WooCommerce store.

== Description ==

This is a WooCommerce integration for the "Software License Manger" plugin. This addon will allow for assigning license keys to digital products at checkout, as well as renewing product licenses.

This plugin will integrate the "Software License Manager" plugin with your WooCommerce Store.

= Please note that this plugin is ONLY for developers =

    1. Product license checkbox now added for "Simple" and "Variable" products.
    2. Variable products also have a domain limit field allowing you to sell one or more site license plugins.
    3. Upon payment completion a license key will be created if the "Product License" checkbox is checked. An email with their order details/download link and license key is sent.
    4. At checkout the user may also enter their license key to renew a plugin they already have.
    5. Renewal email reminders will automatically be sent out at 1 month, 2 weeks & 3 days before expiration date. This renewal email will have their product name and expire date along with a button/link to the checkout page which will then pre populate the checkout with their product and license key. Upon payment for renewal, their license expire date will be updated a year from that day.
    6. Customer account page now has a "License" section which shows the order number and link to the order review, as well as the product name, license key and expire date. Clicking the "+" symbol will expand to show domain(s) activated for that license key.

= Instructions: =

To have the system run a check for license expiration dates and send out emails you will need to install a plugin called "Cronjob Scheduler" by chrispage1, which you can find in the Wordpress Plugin Directory. Once installed, you will see a link to this under "Settings" called "Cronjob Scheduler". Follow the instructions and then reload the page. You will then see a place to add a Cron Action at the bottom.

Enter the following function call "check_license_expire_date" and select how often you want the system to run it. Then click "Create Cronjob" and your all set!

The renewal email that goes out is very generic with a welcome note about a license that is soon to expire, along with a table showing the product and expiration date. Below that is a button with a link to the shopping cart to renew their key.

If you would like to add a header and footer into this email template the go into the plugin folder "slm-woocommerce-addon" and copy the renew-email-template.php file from the "templates" folder and create a folder in your theme called "slm-addon" and place it in there. You can then add header and footer content as well as change/add some verbiage.

== Installation ==

1. Go to the Add New plugins screen in your WordPress admin area
1. Click the upload tab
1. Browse for the plugin file (slm-woocommerce-addon.zip)
1. Click Install Now and then activate the plugin

== Frequently Asked Questions ==
None

== Video Walkthrough ==
youtube https://www.youtube.com/watch?v=ezKe9msEuGg

Update https://www.youtube.com/watch?v=VYi0_msPj1w

== Changelog ==

= 2.0 =
* Major updates to correct functionality and coding errors see new video update walkthrough of changes.

= 1.9 =
* Commented out the WooCommerce auto complete order function as it was uncommented by accident in a commit reversal.

= 1.8 =
* Some code cleanup and languages added as well as fixing the secret key to call from settings in order completion.

= 1.7 =
* Forgot to comment out the "custom_woocommerce_auto_complete_order" function that was used to force complete orders during sandbox testing.

= 1.6 =
* Fixed jquery issue with expanding to see domains activated as well as correcting the column span for the message that appears if no purhcased plugins had licenses.

= 1.5 =
* Added note to license end point under "My Account". If there are no purchased plugins with license keys, then it displays a message telling you so instead of being blank.

= 1.4 =
* Removed fontawesome style sheet from css folder and changed call to fontawesome on bootstrapcdn.com with either http or https.

= 1.3 =
* Changed call to image call to admin menu icon as well as call to styles.css. Also moved added latest FontAwesome to css folder and changed call to from fontawesome.io to the css folder as there were issues with https sites calling to a non https css file.

= 1.2 =
* Removed unused variable in woocommerce-link-to-checkout and corrected post status call for woocommerce that no longer works for version 2.2 in woocommerce-license-endpoint.

= 1.1 =
* Removed unnecessary register_activation hook at the bottom of woocommerce-licens-endpoint.php. Fixed jquery issue $ verses jQuery in woocommerce-link-to-checkout.php as well as removing the session_start(); call, as WooCommerce already has session start. This may have been causing lag issues for some.

= 1.0 =
* First commit to wordpress repository.

== Upgrade Notice ==
None