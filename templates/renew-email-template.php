<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
	<tr>
		<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" width="80%" style="border-collapse: collapse;">
				<tr>
					<td>
						<p><?php _e('Hi there','slm_wooaddon')?> <?php echo $FirstName; ?>,</p>
						<p><strong>We noticed that one or more of your <?php echo bloginfo('name'); ?> subscriptions will expire soon:</strong></p>
										
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr class="header">
								<td width="73%" valign="top"><?php _e('Product','slm_wooaddon') ?></td>
								<td width="27%" valign="top"><?php _e('Expire Date','slm_wooaddon') ?></td>
							</tr>
                            <?php
							$count = 0;
							global $PID, $LKEY;
							$PID[] = array();
							$VID[] = array();
							$LKEY[] = array();
							$VA[] = array();
							foreach($array as $values) {
								$PID[] = $values->product_id;
								$VID[] = $values->variation_id;
								$VA[] = $values->variation_attribute;
								$LKEY[] = $values->license_key;
								 $count++;
								?> 
                                <tr class="details">
                                    <td width="73%" valign="top"><?php echo $values->product_name; ?></td>
                                    <td width="27%" valign="top"><?php echo $values->date_expiry; ?></td>
                                </tr>
					  <?php }// end foreach array ?>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding: 10px 18px 8px 0;">
                    <?php
                    if($count > 1){
						$ProductID = implode(",", $PID);
						$VariationID = implode(",", $VID);
						$License_Key = implode(",", $LKEY);
						$VariationAtt = implode(",", $VA);
						
						if($VariationID == true){
							$Vlink = '&variation_id='.$VariationID.'&attribute_subscription-options='.$VariationAtt;	
						}
                    	?>
                        <a class="renew_button" href="<?php echo site_url(); ?>/checkout/?wc_license_key=<?php echo $License_Key; ?>&add-to-cart=<?php echo $ProductID.$Vlink; ?>"><?php _e('Renew License','slm_wooaddon') ?></a>
                        <?php
					}else{
						if($values->variation_id == true){
							$Vlink2 = '&variation_id='.$values->variation_id.'&attribute_subscription-options='.$values->variation_attribute;
						}
						?>
						<a class="renew_button" href="<?php echo site_url(); ?>/checkout/?wc_license_key=<?php echo $values->license_key; ?>&add-to-cart=<?php echo $values->product_id.$Vlink2; ?>"><?php _e('Renew License','slm_wooaddon') ?></a>	
						<?php
                	}?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>