<?php
class My_Custom_My_Account_Endpoint {
	/**
	 * Custom endpoint name.
	 *
	 * @var string
	 */
	public static $endpoint = 'licenses';
	/**
	 * Plugin actions.
	 */
	public function __construct() {
		load_plugin_textdomain( 'slm_wooaddon', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		// Actions used to insert a new endpoint in the WordPress.
		add_action( 'init', array( $this, 'add_endpoints' ) );
		add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );
		// Change the My Accout page title.
		add_filter( 'the_title', array( $this, 'endpoint_title' ) );
		// Insering your new tab/page into the My Account page.
		add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
		add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );
	}
	/**
	 * Register new endpoint to use inside My Account page.
	 *
	 * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
	 */
	public function add_endpoints() {
		add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
	}
	/**
	 * Add new query var.
	 *
	 * @param array $vars
	 * @return array
	 */
	public function add_query_vars( $vars ) {
		$vars[] = self::$endpoint;
		return $vars;
	}
	/**
	 * Set endpoint title.
	 *
	 * @param string $title
	 * @return string
	 */
	public function endpoint_title( $title ) {
		global $wp_query;
		$is_endpoint = isset( $wp_query->query_vars[ self::$endpoint ] );
		if ( $is_endpoint && ! is_admin() && is_main_query() && in_the_loop() && is_account_page() ) {
			// New page title.
			$title = __( 'Licenses', 'slm_wooaddon' );
			remove_filter( 'the_title', array( $this, 'endpoint_title' ) );
		}
		return $title;
	}
	/**
	 * Insert the new endpoint into the My Account menu.
	 *
	 * @param array $items
	 * @return array
	 */
	public function new_menu_items( $items ) {
		// Remove the logout menu item.
		$logout = $items['customer-logout'];
		unset( $items['customer-logout'] );
		// Insert your custom endpoint.
		$items[ self::$endpoint ] = __( 'Licenses', 'slm_wooaddon' );
		// Insert back the logout item.
		$items['customer-logout'] = $logout;
		return $items;
	}
	/**
	 * ENDPOINT HTML CONTENT.
	 */
	public function endpoint_content() {
		global $order_count;
		$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
	'numberposts' => $order_count,
	'meta_key'    => '_customer_user',
	'meta_value'  => get_current_user_id(),
	'post_type'   => 'shop_order',
	'post_status' => array( 'wc-completed' )
) ) );

if ( $customer_orders ) : ?>
	<div><?php echo sprintf(__( 'Click %s to expand and see active domains.', 'slm_wooaddon' ),'<i class="fa fa-plus-circle" aria-hidden="true"></i>'); ?></div>
	<table class="shop_table my_account_orders" width="100%">

		<thead>
			<tr>
				<th class="order-number"><span class="nobr"><?php _e( 'Order', 'slm_wooaddon' ); ?></span></th>
				<th class="order-item"><span class="nobr"><?php _e( 'Item', 'slm_wooaddon' ); ?></span></th>
				<th class="order-license"><span class="nobr"><?php _e( 'License Key', 'slm_wooaddon' ); ?></span></th>
				<th class="order-license"><span class="nobr"><?php _e( 'Sites', 'slm_wooaddon' ); ?></span></th>
			</tr>
		</thead>

		<tbody><?php
			foreach ( $customer_orders as $customer_order ) {
				$order = new WC_Order( $customer_order );

				//$order->populate( $customer_order );

				$status     = get_term_by( 'slug', $order->get_status(), 'shop_order_status' );
				$item_count = $order->get_item_count();
				$orderNumber = $order->get_order_number();
				
				foreach($order->get_items() as $item) {
					//var_dump($item);
					$product_name = $item['name'];
					$product_id = $item['product_id'];
					// $meta_data = $item->get_meta_data();
					// foreach( $meta_data as $meta_item ) {
					// 	var_dump( $meta_item->value );
					// }
				

					global $wpdb;
					$license_table = SLM_TBL_LICENSE_KEYS; 
					$prepare_query = $wpdb->prepare("SELECT * FROM " . $license_table . " WHERE txn_id = ".$orderNumber." AND product_id = %d","".$product_id );
	
					$data = $wpdb->get_results($prepare_query);
					foreach($data as $key=> $dat){

						$license_key = $dat->license_key;
						$max_domains = $dat->max_allowed_domains;
						$product_number = $dat->product_id;
						
						
						if($license_key){
							?>
							<tr class="order">
								<td class="order-number">
									<a href="<?php echo site_url().'/my-account/view-order/'.$orderNumber; ?>">
										<?php echo "#" .$order->get_order_number(); ?>
									</a>
								</td>
								<td class="order-item">
									<?php echo $product_name;?>
								</td>
								<td class="order-license" style="text-align:left; white-space:nowrap;">
									<?php echo $license_key; ?>
								</td>
								<td class="order-license" style="text-align:left; white-space:nowrap;">
									<?php echo $max_domains; ?> <div class="plus"><i class="fa fa-plus-circle" aria-hidden="true"></i>
		</div>
								</td>
								
							</tr>
							<?php
								//Show domains activated
								global $wpdb;
								$reg_table = SLM_TBL_LIC_DOMAIN;
		
								$sql_prep = $wpdb->prepare("SELECT * FROM wp_lic_reg_domain_tbl WHERE lic_key = %s", $license_key);
		
								$reg_domains = $wpdb->get_results($sql_prep, OBJECT);
								?>
								<tr class="domains">
									<td colspan="4">
										<strong>Active Domains:</strong> 
										<?php
										foreach ($reg_domains as $reg_domain) {
											echo $reg_domain->registered_domain.' ';
										}
										?>
									</td>
								</tr>
									<?php
							
						}//end if $license_key
					}//end foreach $data
				}//end foreach $order
			}// end foreach $customer_orders as $customer_order
		?></tbody>

	</table>

	<script>
		jQuery('.plus').click(function(event) {
			event.preventDefault();
			jQuery(this).closest('tr').next('.domains').toggle();
			jQuery(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
		});
	</script>
<?php endif;// end if $customer_orders

	}
	/**
	 * Plugin install action.
	 * Flush rewrite rules to make our custom endpoint available.
	 */
	public static function install() {
		flush_rewrite_rules();
	}
}
new My_Custom_My_Account_Endpoint();
?>